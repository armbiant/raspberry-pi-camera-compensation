# Keyboard, Monitor, and Mouse

In order to use the Raspberry Pi, you will need a USB keyboard and mouse, and a monitor with an HDMI (or micro-HDMI for the Pi 4) connector.  Alternatively, you can run your Pi "headless" and connect with a wired or wireless network connection; instructions can be found online for this.
