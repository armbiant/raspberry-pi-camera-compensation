# Soldering iron

A soldering iron is probably needed in order to connect the [wires](wires.md) to the [NeoPixel](neopixel.md).  Any soldering iron should do - this is fairly basic soldering onto the pads of the NeoPixel's flex.
