# White paper

A small (a few cm square) piece of white paper is used as a diffuser.  There are no special requirements - other than you might need to cut or tear it to fit between the screws.  Normal 80gsm printer paper is what we used, but it shouldn't be too sensitive.
