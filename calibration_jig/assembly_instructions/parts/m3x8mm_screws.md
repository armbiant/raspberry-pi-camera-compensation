# M3x8mm screws

## Details
*   **Supplier:** Anglian Fasteners Limited
*   **Supplier's part number:** 14315
*   **URL:** http://www.anglianfasteners.co.uk/
*   **Material units:** NONE

These screws attach the various parts of the jig together.  They don't need to be particularly fine quality, and the type of head is not particularly important.  We usually use either hex socket head or button head so they can be easily driven using a [hex key](./2_5mm_hex_key.md).  NB the bill of materials specifies a 2.5mm hex key; if you use button head screws, you may need a 2mm key instead, and if you use a different head type you will need the appropriate screwdriver.



