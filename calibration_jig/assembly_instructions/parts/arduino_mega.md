# Arduino Mega

We used an [Arduino Mega](https://store.arduino.cc/arduino-mega-2560-rev3) to control the [NeoPixel](neopixel.md).  This was because we had one available in the lab - almost any Arduino would do, though you might need to tweak the sketch slightly, or use different pins.  These can be obtained from many different suppliers, for example [RS Components](https://uk.rs-online.com/web/p/products/7154084).

NB we have assumed that your Arduino comes with an appropriate USB cable, for power and control.  If you buy an unofficial clone, it may not - so you'll need to order one.
