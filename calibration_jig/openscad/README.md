# PiCamera CRA compensation calibration jig

This folder holds the OpenSCAD designs for a simple mechanical assembly that allows you to illuminate a Raspberry Pi camera module with light of different colours, from a variable angle.  By taking measurememts with the sensor at different angles, it should be possible to figure out the optimal angle of incidence at each point on the sensor.  It should also be a convenient way to calibrate the sensor to take flat-field images, by generating a lens shading table and/or post-processing correction matrix.  There is only one main OpenSCAD source file, but by commenting out different functions, it can be used to build all of the STL files from the functions with the same names.

The jig consists of two parts: a tube (into which the LED is mounted) and a holder for the Raspberry Pi camera module.  You may recognise quite a few of the OpenSCAD functions from the [OpenFlexure microscope](https://github.com/rwb27/openflexure_microscope/).

The imaging system described in the manuscript, which uses a f=50mm lens, is the infinity-corrected version of the OpenFlexure Microscope optics module, obtainable from the repository above.  We intend to archive a copy of the OpenFlexure Microscope source along with the paper, to guard against changes upstream.  However, we have also copied in the relevant STL file for convenience.
