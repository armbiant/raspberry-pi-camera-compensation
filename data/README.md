# Image data
This folder contains the image data acquired in two experiments.  Each run is saved as a folder of JPEG images.  Each JPEG image has embedded EXIF metadata, which includes the raw output of the Raspberry Pi camera's sensor.  It is this raw data (and not the JPEG image data) that is used in the manuscript.  Each folder also includes the camera settings in YAML format (this can be read as a text file), a calculated lens shading table (also in YAML format), and a PDF document providing a preview of the raw images.  NB the preview may not match the content of the JPEG images; this is because the JPEG images have been processed by the camera's image processing pipeline, while the preview images have not.

``calibration_jig_r3`` includes two sets of images, taken using the calibration jig described in the manuscript, at normal incidence and tilted.

``monitor_and_50mm_lens`` includes a number of datasets taken using both the calibration jig and an imaging system based on a 50mm focal length achromatic lens.  Within this folder, each dataset corresponds to a different set-up:
* ``flat_lst`` is a sequence of images acquired using a screen and 50mm lens, with a flat lens shading table (i.e. the JPEG images have not been corrected for vignetting).
* ``with_lst`` is a repeat of the ``flat_lst`` experiment, using the lens shading table calculated from the first run.  The JPEG images should be corrected for vignetting, but the raw data are essentially identical to the first run.
* ``neopixel`` was acquired using the neopixel mounted next to a diffuser, at the entrance aperture of the 50mm lens.
* ``neopixel_jig`` was acquired using the calibration jig as described in the manuscript.
* ``tape_aperture_1`` and ``tape_aperture_2_diagonal`` were repeats of ``flat_lst`` but with a square aperture added, just after the 50mm lens.  This was intended to reduce the spread of angles of incidence, but in practice the only noticable effect was a decrease in intensity.
