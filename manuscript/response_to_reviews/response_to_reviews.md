# Response to reviewer comments on "Flat-field and colour correction for the Raspberry Pi camera module"
We would like to thank both reviewers for positive and constructive comments on our manuscript.  We have done our best to address their points below, and include a revised manuscript that incorporates our changes in response.

## Reviewer A
### Availability of design files
> Does the Overall Implementation and Design section give enough information to get an idea of how the hardware is designed, and any constraints that may be placed on its use?
> Yes, although I had to look at the authors' source repository (link provided) for some details.

We are pleased that the reviewer was able to find everything, and as no specific point is made regarding information that should be in the manuscript, rather than the repository, we have not made any changes in response.  If there is information that ought to be moved from the repository into the manuscript, we are happy to do so.

### Algebraic expression for the correction
> Because you are correcting a purely geometrical effect, can a rough calibration be done offline (i.e. algebraically) given the focal length and field of view of the lens?  If this is possible, even if only approximate, it would be useful for users who may not have the equipment or time to build the calibration jig or take the calibration data.  Having an algebraic expression would also give you a point of comparison for your empirical calibration.

This is something we've spent a little time looking into, and currently our conclusion is that we couldn't derive a mathematical model that accurately reflects the data.  The effect of the lenslets doesn't seem to be matched particularly well by any functions we could guess, though we did try a couple of promising functional forms that worked to around 4% (when we optimised a few free parameters).  We have included our work to fit the correction with an equation in a supplementary notebook, ``modelling.ipynb``, which is included in the ``analysis`` folder of our data archive and [viewable online via gitlab](https://gitlab.com/bath_open_instrumentation_group/picamera_cra_compensation/blob/master/analysis/modelling.ipynb).  While we are not convinced this work merits inclusion in the body of the paper in much detail, it is now available for inspection and we have mentioned it in the text.

### Saturation
> I do have some concern about your statements regarding saturation.  You mention that using the fully color mixing matrix yields an oversaturated image.  However, your source image file is itself fully saturated, so why wouldn't you expect the corrected camera photo to be fully saturated?  You do comment on the color unmixing generating excess noise, which is a relevant point, but the saturation argument itself needs clarification.

While the source image file is fully saturated, I would not expect a photograph of a monitor displaying said image to be fully-saturated.  Most light sources, including the LEDs and monitor used in this work, emit a range of wavelengths.  Similarly, the red, green, and blue filters in the Bayer pattern on the sensor are not perfect band-pass filters but have some amount of overlap.  This means that, even with a perfectly uniform sensor, we would expect a fully-saturated image, when displayed on a real screen and imaged with a Bayer filter, to be detected with less than full saturation.  We have added a couple of sentences to the manuscript that we hope clarifies this point.

## Reviewer B
### Discussion
> The writing is very clear, however the authors have not completed the
> discussion section. The authors should comment on the usefulness of
> their system and possible modifications/extensions.

We would like to thank the reviewer for pointing this out.  We have added sections on conclusions and future work, addressing the points raised.

### Pi Camera figure
> Include a figure of the Pi camera to indicate size and stock lens arrangement

We have added this as Figure 1.

### References
> Make sure all libraries and documentation are fully referenced, e.g. with URLs

While the URLs were present in our source bibliography, they were not being formatted correctly by the bibliography software - we have fixed this.

### Quality Control section
> There is no Quality Control section

We did spend some time considering how we could add a quality control section.  However, the whole manuscript deals with the challenge of calibrating a camera - which we would expect to become part of the quality control procedure in other hardware projects.  We have also analysed the signal to noise penalty of our correction and included code to allow others to do the same.  We hope this is sufficient attention to quality control; while we can add a section with that name if it is required, I'm not sure it will result in any meaningful content being added to the manuscript.

### Re-use
> In the re-use section, think about whether it could be used for other things, e.g. other low cost cameras

We have added a statement mentioning the possibility of performing this calibration on other cameras - the referee is right to highlight the potential applicability to other low cost cameras.