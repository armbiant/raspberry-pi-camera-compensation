#Open know-how manifest 1.0-valid
---

# The content of this manifest file is licensed under a Creative Commons Attribution 4.0 International License.
# Licenses for modification and distribution of the hardware, documentation, source-code, etc are stated separately.

# Remove any fields that are not used. Comments (beginning with '#') may also be removed.

# Manifest metadata

date-created: 2019-10-17

date-updated: 2019-10-17

manifest-author:
  name: Richard Bowman
  affiliation: University of Bath
  email: r.w.bowman@bath.ac.uk

manifest-language: en-GB

documentation-language: en

# Properties

title: Raspberry Pi Camera Calibration Jig

description: |                             # required | paragraph
  A 3D-printed jig to uniformly illuminate the Raspberry Pi camera module's sensor, for the purpose of calibrating its response to different colours of illumination.  This enables flat-field correction, and also the unmixing of colour channels.  As the off-centre lenslet array means that (with uniform angle-of-incidence) the pixels at the edge suffer from significant crosstalk relative to the centre, correcting for varying colour response results in much truer colour representation.

intended-use: |                            # recommended | Paragraph
  Calibrating the Raspberry Pi camera module for colour response.

keywords:                                  # At least one keyword is recommended | text array
  - colorimetry
  - calibration
  - camera module
  - raspberry pi

project-link: https://gitlab.com/bath_open_instrumentation_group/picamera_cra_compensation/                      # At least project-link or documentation-home is required, otherwise recommended | absolute path URL

health-safety-notice: |                    # paragraph
  No major health and safety issues have been identified.

contact:
  name: Richard Bowman                            # text
  affiliation: University of Bath                     # text
  email: r.w.bowman@bath.ac.uk                           # email address
  social:
    - platform: twitter
      user-handle: mindnumbed

contributors:                              # recommended
  - name: Richard Bowman                            # text
    affiliation: University of Bath                     # text
    email: r.w.bowman@bath.ac.uk                           # email address

image: ./calibration_jig/assembly_instructions/images/jig_assembled.jpg

version: 1.0.0                          # text

license:
  hardware: CERN-OHL-1.2
  documentation: CC-BY-4.0
  software: GPL-3.0-or-later

made: true                         # boolean - true or false

made-independently: false         # boolean - true or false

# Documentation

documentation-home: https://bath_open_instrumentation_group.gitlab.io/picamera_cra_compensation/              # At least one of the project-link or documentation-home fields is required | absolute path

archive-download: https://gitlab.com/bath_open_instrumentation_group/picamera_cra_compensation/-/archive/master/picamera_cra_compensation-master.zip

design-files:                             # recommended
  - path: ./calibration_jig/openscad/                         # Absolute or relative path
    title: OpenSCAD source files                        # text

schematics:                               # recommended where applicable
  - path: ./manuscript/artwork/apparatus.pdf
    title: Diagram of the apparatus                        # text

bom: ./calibration_jig/assembly_instructions/calibration_jig.md

tool-list: ./calibration_jig/assembly_instructions/calibration_jig.md

making-instructions:                      # recommended
  - path: ./calibration_jig/assembly_instructions/calibration_jig.md                        # Absolute or relative path
    title: Assembly instructions

manufacturing-files:                      # recommended where applicable
  - path: ./calibration_jig/stl/
    title: STL files for printing

operating-instructions:                   # recommended
  - path: ./manuscript                         # Absolute or relative path
    title: LaTeX format manuscript, describing usage and results               # text

software:                                 # recommended where applicable | Source code or executable software that the thing uses
  - path: ./image_acquisition/                         # Absolute or relative path
    title: Python control scripts                        # text
  - path: ./neopixel_driver
    title: Arduino firmware

# User defined Fields
# Include any custom / extended fields here