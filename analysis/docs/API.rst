

picam\_raw\_analysis package
============================
Module contents
---------------

.. automodule:: picam_raw_analysis
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

picam\_raw\_analysis.extract\_raw\_image module
-----------------------------------------------

.. automodule:: picam_raw_analysis.extract_raw_image
   :members:
   :undoc-members:
   :show-inheritance:

picam\_raw\_analysis.lst\_from\_raw\_white\_image module
--------------------------------------------------------

.. automodule:: picam_raw_analysis.lst_from_raw_white_image
   :members:
   :undoc-members:
   :show-inheritance:


picam\_raw\_analysis.unmix\_image module
----------------------------------------

.. automodule:: picam_raw_analysis.unmix_image
   :members:
   :undoc-members:
   :show-inheritance:

picam\_raw\_analysis.unmixing\_matrix module
--------------------------------------------

.. automodule:: picam_raw_analysis.unmixing_matrix
   :members:
   :undoc-members:
   :show-inheritance:


picam\_raw\_analysis.dump\_exif module
--------------------------------------

.. automodule:: picam_raw_analysis.dump_exif
   :members:
   :undoc-members:
   :show-inheritance:


picam\_raw\_analysis.picamera\_array module
-------------------------------------------

The picamera\_array module is lifted more or less directly from the `picamera` module.  It has been modified to remove dependencies on the rest of `picamera`, and has been extended to include some additional array types, notably `picam\_raw\_analysis.picamera\_array.PiSharpBayerArray` (which implements minimally smoothed debayering).

.. automodule:: picam_raw_analysis.picamera_array
   :members:
   :undoc-members:
   :show-inheritance:
   

picam\_raw\_analysis.mo\_stub module
------------------------------------

This module exists only for convenience, to allow the dummy picamera\_array module to load with a minimum of modified code.